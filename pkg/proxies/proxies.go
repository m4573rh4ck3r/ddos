package proxies

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

func GetProxies(proxyType, anon string) ([]string, error) {
	url := fmt.Sprintf("https://www.proxy-list.download/api/v1/get?type=%s&anon=%s", proxyType, anon)

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, fmt.Errorf("failed to create request: %v", err)
	}

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, fmt.Errorf("failed to do request: %v", err)
	}

	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to read response body: %v", err)
	}

	proxies := strings.Split(string(body), "\r")
	// for i, proxy := range proxies {
	// 	proxies[i] = fmt.Sprintf("'%s'", strings.TrimPrefix(proxy, "\n"))
	// 	proxies[i] = fmt.Sprintf("'%s'", strings.TrimSuffix(proxy, "\n"))

	// 	proxies[i] = fmt.Sprintf("'%s'", strings.TrimPrefix(proxy, "\\n"))
	// 	proxies[i] = fmt.Sprintf("'%s'", strings.TrimSuffix(proxy, "\\n"))

	// 	proxies[i] = fmt.Sprintf("'%s'", strings.TrimPrefix(proxy, "\r"))
	// 	proxies[i] = fmt.Sprintf("'%s'", strings.TrimSuffix(proxy, "\r"))

	// 	proxies[i] = fmt.Sprintf("'%s'", strings.TrimPrefix(proxy, "\\r"))
	// 	proxies[i] = fmt.Sprintf("'%s'", strings.TrimSuffix(proxy, "\\r"))
	// }
	proxies = proxies[:len(proxies)-1]

	return proxies, nil
}
