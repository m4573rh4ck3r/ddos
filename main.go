package main

import (
	"log"

	"gitlab.com/m4573rh4ck3r/ddos/cmd/ddos"
)

func main() {
	if err := ddos.Execute(); err != nil {
		log.Fatal(err)
	}
}
