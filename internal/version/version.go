package version

import (
	"fmt"
	"runtime"
)

var (
	Version     = ""
	GitRevision = ""
	GitBranch   = ""
	GoVersion   = runtime.Version()
	OsArch      = fmt.Sprintf("%s/%s", runtime.GOOS, runtime.GOARCH)
	BuildTime   = ""
)

// --------------------------------------------------
// detailed version output template
// --------------------------------------------------
var DetailedVersionTemplate = `%s
Version:      %s
Git revision: %s
Git branch:   %s
Go version:   %s
Build time:   %s
OS/Arch:      %s
`
