package cmd

import (
	"fmt"

	"github.com/spf13/cobra"

	"gitlab.com/m4573rh4ck3r/ddos/internal/version"
)

var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "show detailed version output",
	Args:  cobra.NoArgs,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Printf(version.DetailedVersionTemplate, rootCmd.Use, version.Version, version.GitRevision, version.GitBranch, version.GoVersion, version.BuildTime, version.OsArch)
	},
}
