FROM golang as builder
ADD . /go/src/gitlab.com/m4573rh4ck3r/ddos
WORKDIR /go/src/gitlab.com/m4573rh4ck3r/ddos
RUN make build

FROM debian:stable-slim
COPY --from=builder /ddos /
RUN apt-get update && \
	apt-get install -y --no-install-recommends \
		apt-transport-https \
		ca-certificates \
		libc6
